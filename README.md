# Deep Learning: Coursera

**Master Deep Learning, and Break into AI**

Instructor: [Andrew Ng](http://www.andrewng.org/)

## Certificate
I got full credits in all five courses of this specialization.
[Here](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Certificate_Jianren_Wang.pdf) is my certificate and can be verified through [this link](https://www.coursera.org/account/accomplishments/specialization/PNY3MNNWJTJU) 


## Note
According to coursera honor code, all materials are not shareable. If you are currently viewing this repo, please note: copyrights belongs to coursera. 

## Assignments

- Course 1: Neural Networks and Deep Learning

  - [Week 2 - Logistic Regression with a Neural Network mindset](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Neural%20Networks%20and%20Deep%20Learning/Logistic%20Regression%20with%20a%20Neural%20Network%20mindset.ipynb)
  - [Week 3 - Planar data classification with one hidden layer](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Neural%20Networks%20and%20Deep%20Learning/Planar%20data%20classification%20with%20one%20hidden%20layer.ipynb)
  - [Week 4 - Building your Deep Neural Network: Step by Step](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Neural%20Networks%20and%20Deep%20Learning/Building%20your%20Deep%20Neural%20Network%20-%20Step%20by%20Step.ipynb)
  - [Week 4 - Deep Neural Network for Image Classification: Application](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Neural%20Networks%20and%20Deep%20Learning/Deep%20Neural%20Network%20-%20Application.ipynb)

- Course 2: Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization

  - [Week 1 - Initialization](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Initialization.ipynb)
  - [Week 1 - Regularization](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Regularization.ipynb)
  - [Week 1 - Gradient Checking](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Gradient%20Checking.ipynb)
  - [Week 2 - Optimization Methods](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Optimization%20methods.ipynb)
  - [Week 3 - TensorFlow Tutorial](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Improving%20Deep%20Neural%20Networks%20Hyperparameter%20tuning,%20Regularization%20and%20Optimization/Tensorflow%20Tutorial.ipynb)

- Course 3: Structuring Machine Learning Projects

  - Case study quizzes.
  
- Course 4: Convolutional Neural Networks

  - [Week 1 - Convolutional Model: step by step](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Convolutional%20Neural%20Networks/Convolution%20model%20-%20Step%20by%20Step%20-%20v1.ipynb)
  - [Week 1 - Convolutional Model: application](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Convolutional%20Neural%20Networks/Convolution%20model%20-%20Application%20-%20v1.ipynb)
  - [Week 2 - Keras - Tutorial - Happy House](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Convolutional%20Neural%20Networks/Keras%20-%20Tutorial%20-%20Happy%20House%20v1.ipynb)
  - [Week 2 - Residual Networks](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Convolutional%20Neural%20Networks/Residual%20Networks%20-%20v1.ipynb)
  
- Course 5: Sequence Models

  - [Week 1 - Building a Recurrent Neural Network - Step by Step](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Sequence%20Models/Building%20a%20Recurrent%20Neural%20Network%20-%20Step%20by%20Step%20-%20v2.ipynb)
  - [Week 1 - Dinosaurus land - Character level language model](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Sequence%20Models/Dinosaurus%20Island%20--%20Character%20level%20language%20model%20final%20-%20v3.ipynb)
  - [Week 1 - Jazz improvisation with LSTM](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Sequence%20Models/Improvise%20a%20Jazz%20Solo%20with%20an%20LSTM%20Network%20-%20v1.ipynb)
  - [Week 2 - Operations on word vectors](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Sequence%20Models/Operations%20on%20word%20vectors%20-%20v2.ipynb)
  - [Week 2 - Emojify](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Sequence%20Models/Emojify%20-%20v2.ipynb)
  - [Week 3 - Neural machine translation with attention](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Sequence%20Models/Neural%20machine%20translation%20with%20attention%20-%20v2.ipynb)
  - [Week 3 - Trigger word detection](https://bitbucket.org/Jianren_Wang/deep-learning-coursera/src/master/Sequence%20Models/Trigger%20word%20detection%20-%20v1.ipynb)

## Quiz Solutions

There are concerns that some people may use the content here to quickly ace the course so I'll no longer update any quiz solution.

- Course 1: Neural Networks and Deep Learning

  - [Week 1 - Introduction to deep learning]()
  - [Week 2 - Neural Network Basics]()
  - [Week 3 - Shallow Neural Networks]()
  - [Week 4 - Key concepts on Deep Neural Networks]()

- Course 2: Improving Deep Neural Networks: Hyperparameter tuning, Regularization and Optimization

  - [Week 1 - Practical aspects of deep learning]()
  - [Week 2 - Optimization algorithms]()
  - [Week 3 - Hyperparameter tuning, Batch Normalization, Programming Frameworks]()
  
- Course 3: Structuring Machine Learning Projects

  - [Week 1 - Bird recognition in the city of Peacetopia (case study)]()
  - [Week 2 - Autonomous driving (case study)]()


